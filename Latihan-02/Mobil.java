public class Mobil extends Kendaraan {
    protected String bahanBakar;
    protected int kapasitasMesin;

    public void setBahanBakar(String bahanString) {
        this.bahanBakar = bahanString;
    }

    public String getBahanBakar() {
        return this.bahanBakar;
    }

    public void setKapasitasMesin(String kapasitasMesin) {
        this.kapasitasMesin = Integer.parseInt(kapasitasMesin);
    }

    public int getKapasitasMesin() {
        return this.kapasitasMesin;
    }
}
