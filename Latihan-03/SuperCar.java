class SuperCar extends Car{
    private double topSpeed;

    public SuperCar(String name, double price, double topSpeed){
        super(name, price);
        this.topSpeed = topSpeed;
    }

    public double getTopSpeed(){
        return this.topSpeed;
    }

    public void setTopSpeed(double topSpeed){
        this.topSpeed = topSpeed;
    }
}
