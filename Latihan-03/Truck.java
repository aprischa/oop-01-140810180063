class Truck extends Car{
    private double maxCapacity;
    
    public Truck(String name, double price, double maxCapacity){
        super(name,price);
        this.maxCapacity = maxCapacity;
    }

    public double getMaxCapacity(){
        return this.maxCapacity;
    }

    public void setMaxCapacity(double maxCapacity){
        this.maxCapacity = maxCapacity;
    }
}
