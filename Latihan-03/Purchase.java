class Purchase{
    private Buyer buyers;
    private Car[] car = new Car[10];
    private int totalPurchase = 0;;

    public Purchase(Buyer buyer){
        this.buyers = buyer;
        this.totalPurchase = 0;
    }

    public void addCar(Car car){
        this.car[this.totalPurchase] = car;
        this.totalPurchase++;
    }

    public int getTotalPurchase(){
        return this.totalPurchase;
    }

    public void printPurchase(){
        System.out.println("Mr./Mrs. " + this.buyers.getName());
        System.out.println(this.buyers.getAddress() + "\n---");

        for(int i=0;i<this.totalPurchase;i++){
            System.out.println((i+1) + ". " + this.car[i].getName());
            System.out.println("Price\t\t$" + this.car[i].getPrice());
            if(car[i] instanceof SuperCar){
                SuperCar supercar = (SuperCar)car[i];
                System.out.println("Top speed : \t" + supercar.getTopSpeed());
            }
            else{
                
                Truck truck = (Truck)car[i];
                System.out.println("Max Capacity : \t" + truck.getMaxCapacity());
            }
        }
    }
}
