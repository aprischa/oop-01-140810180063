class Main{
    public static void main(String[] args) {
        Buyer buyer = new Buyer("Miun", "Pasirlayung, Bandung");
        Purchase prchs = new Purchase(buyer);

        Car car = new Truck("Hino dutro", 120000, 5000);
        prchs.addCar(car);

        car = new SuperCar("Lamborghini", 2000000, 350);
        prchs.addCar(car);
        prchs.printPurchase();
    }
}
